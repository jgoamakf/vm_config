loadkeys jp106

parted /dev/sda mklabel gpt
parted /dev/sda mkpart BOOT fat32 1MiB 261MiB
parted /dev/sda mkpart SWAP linux-swap 261MiB 1285MiB
parted /dev/sda mkpart ROOT xfs 1285MiB 100%
parted /dev/sda set 1 esp on 

mkfs.vfat /dev/sda1
mkswap /dev/sda2
mkfs.xfs /dev/sda3

mount /dev/sda3 /mnt
swapon /dev/sda2
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
pacstrap /mnt base linux linux-firmware

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt

