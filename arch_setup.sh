pacman -S emacs-nox vim openssh git tmux xfsprogs man-db man-pages texinfo zsh
ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
hwclock --systohc

cat >> /etc/locale.gen <<EOS
en_US.UTF-8 UTF-8
ja_JP.UTF-8 UTF-8
EOS

locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "KEYMAP=jp106" > /etc/vconsole.conf

echo "arch-vbox" > /etc/hostname

cat >> /etc/hosts <<EOS
127.0.0.1	localhost
::1		localhost
127.0.0.1	arch-vbox.localdomain	arch-vbox
EOS

bootctl install
cat > /boot/loader/entries/arch.conf <<EOS
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=/dev/sda3 rw
EOS

cat > /boot/loader/loader.conf <<EOS
default arch
timeout 3
editor no
EOS

cat > /etc/systemd/network/10-enp0s3.network <<EOS
[Match]
Name=enp0s3

[Network]
DHCP=yes
MulticastDNS=yes
LLMNR=no
EOS

cat > /etc/systemd/network/20-enp0s8.network <<EOS
[Match]
Name=enp0s8

[Network]
Address=192.168.56.11/24
MulticastDNS=yes
LLMNR=no
EOS

systemctl enable systemd-networkd
systemctl enable systemd-resolved
systemctl enable sshd

useradd -m hiramatsu
chpasswd <<EOS
root:hiramatsu
hiramatsu:hiramatsu
EOS


